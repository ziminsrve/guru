﻿namespace GURU_VOPROS_OTVETS
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddQ = new System.Windows.Forms.Button();
            this.tbQ = new System.Windows.Forms.TextBox();
            this.btnAddA = new System.Windows.Forms.Button();
            this.tbA = new System.Windows.Forms.TextBox();
            this.lbQ = new System.Windows.Forms.ListBox();
            this.lbA = new System.Windows.Forms.ListBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.tbNum = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // AddQ
            // 
            this.AddQ.Location = new System.Drawing.Point(371, 389);
            this.AddQ.Name = "AddQ";
            this.AddQ.Size = new System.Drawing.Size(163, 49);
            this.AddQ.TabIndex = 0;
            this.AddQ.Text = "Добавить вопрос";
            this.AddQ.UseVisualStyleBackColor = true;
            this.AddQ.Click += new System.EventHandler(this.AddQ_Click);
            // 
            // tbQ
            // 
            this.tbQ.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbQ.Location = new System.Drawing.Point(45, 340);
            this.tbQ.Name = "tbQ";
            this.tbQ.Size = new System.Drawing.Size(814, 34);
            this.tbQ.TabIndex = 1;
            // 
            // btnAddA
            // 
            this.btnAddA.Location = new System.Drawing.Point(578, 257);
            this.btnAddA.Name = "btnAddA";
            this.btnAddA.Size = new System.Drawing.Size(163, 49);
            this.btnAddA.TabIndex = 0;
            this.btnAddA.Text = "Добавить ответ";
            this.btnAddA.UseVisualStyleBackColor = true;
            this.btnAddA.Click += new System.EventHandler(this.btnAddA_Click);
            // 
            // tbA
            // 
            this.tbA.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbA.Location = new System.Drawing.Point(468, 208);
            this.tbA.Name = "tbA";
            this.tbA.Size = new System.Drawing.Size(391, 34);
            this.tbA.TabIndex = 1;
            // 
            // lbQ
            // 
            this.lbQ.FormattingEnabled = true;
            this.lbQ.ItemHeight = 16;
            this.lbQ.Location = new System.Drawing.Point(45, 30);
            this.lbQ.Name = "lbQ";
            this.lbQ.Size = new System.Drawing.Size(391, 276);
            this.lbQ.TabIndex = 2;
            this.lbQ.SelectedIndexChanged += new System.EventHandler(this.lbQ_SelectedIndexChanged);
            // 
            // lbA
            // 
            this.lbA.FormattingEnabled = true;
            this.lbA.ItemHeight = 16;
            this.lbA.Location = new System.Drawing.Point(468, 30);
            this.lbA.Name = "lbA";
            this.lbA.Size = new System.Drawing.Size(391, 148);
            this.lbA.TabIndex = 2;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(138, 450);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(174, 34);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbNum
            // 
            this.tbNum.Location = new System.Drawing.Point(468, 257);
            this.tbNum.Name = "tbNum";
            this.tbNum.Size = new System.Drawing.Size(66, 22);
            this.tbNum.TabIndex = 4;
            this.tbNum.Text = "6";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(887, 508);
            this.Controls.Add(this.tbNum);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lbA);
            this.Controls.Add(this.lbQ);
            this.Controls.Add(this.tbA);
            this.Controls.Add(this.tbQ);
            this.Controls.Add(this.btnAddA);
            this.Controls.Add(this.AddQ);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AddQ;
        private System.Windows.Forms.TextBox tbQ;
        private System.Windows.Forms.Button btnAddA;
        private System.Windows.Forms.TextBox tbA;
        private System.Windows.Forms.ListBox lbQ;
        private System.Windows.Forms.ListBox lbA;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox tbNum;
    }
}

