﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GURU_VOPROS_OTVETS
{
    public partial class Form1 : Form
    {
        Dictionary<string, List<string>> d = new Dictionary<string, List<string>>();
        public Form1()
        {
            InitializeComponent();
        }

        private void AddQ_Click(object sender, EventArgs e)
        {
            if (tbQ.Text.Trim() == "") return;
            d.Add(tbQ.Text, new List<string>());
            lbQ.Items.Add(tbQ.Text);
            tbQ.Text = "";
        }

        private void lbQ_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbA.Items.Clear();
            foreach (var s in d[lbQ.SelectedItem.ToString()])
            {
                lbA.Items.Add(s);
            }
        }

        private void btnAddA_Click(object sender, EventArgs e)
        {
            if (tbA.Text.Trim() == "") return;
            d[lbQ.SelectedItem.ToString()].Add(tbA.Text);
            lbA.Items.Add(tbA.Text);
            tbA.Text = "";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            List<string> l = new List<string>();
            List<string> l1 = new List<string>();

            for (int i = 0; i < d.Count; i++)
            {
                l.Add((i + 1) + "| " + d.ElementAt(i).Key + " |");
                
                for (int j = 0; j < int.Parse(tbNum.Text); j++)
                {
                    if (j >= d.ElementAt(i).Value.Count)
                    {
                        l1.Add((i + 1) + "| |");
                        continue;
                    }

                    l1.Add((i+1) +"|" + (j+1) + ". " + d.ElementAt(i).Value[j] + " |");
                }
            }

            File.WriteAllLines("VOPROS.txt", l, Encoding.GetEncoding(866));
            File.WriteAllLines("OTVETS.txt", l1, Encoding.GetEncoding(866));

        }
    }
}
